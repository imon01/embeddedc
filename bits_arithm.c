



#include <stdio.h>

#include <limits.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>


void b_swap(int, int);
void b_set(int, int);
void b_add( int, int);
void b_sub(int, int);
void b_mul(int, int);
void b_div(int, int);


void check_endian();	
void check_endian2();	
void check_endian3();	






bool endian(void);

unsigned int count_bits(int);

uint32_t ntohl(uint32_t);
uint32_t nthol_32(uint32_t netlong);

uint32_t bswap32_wrapper(uint32_t);


int main(int argc, char *argv[])
{

//	uint32_t value = 0xAABBCCDD;
//
//	printf("ORIG: %u \n", value);	
//
//	value = __builtin_bswap32(value);
//	
//	printf("BE: %u \n", value);	
//
//	
//	value = ntohl(value);	
//
//
//	printf("LE: %u \n", value);	
//
	if( endian())
		printf("LE\n");
	else
		printf("BE\n");


	b_set(10, 3);

	b_add(1,2);
	b_sub(1,2);
	b_mul(2,2);
	b_div(9,3);

}


/* ----------------------------------------------------------------------------------- */
/* --------------------------------Bits Arithmetic------------------------------------ */
/* ----------------------------------------------------------------------------------- */


void b_set( int a, int i)
{
/*

3 2 1 0
8 4 2 1

*/
	int store_i = i;
	if( i < 0)
		return;
	if( i > sizeof(a)*8) 
		return;

	int mask  = 1;
    mask <<=i;

	while( i)
	{
		mask<<=1;
		i--;
	}

	printf("a=%d, a[%d]= %d set\n", a, store_i, (a ^ mask));
 
}
void  b_swap( int a, int b)
{
	a = a ^ b;
	b = b ^ a;
	a = a ^ b;	
}


void b_add( int a, int b)
{
	int c = 0;
	printf("a=%d, b%d", a, b);



	while (b != 0){
        c = (a & b) ;

        a = a ^ b; 

        b = c << 1;
	}

	

	printf(" a+b=%d\n", a);
}
void b_sub(int a, int b) 
{
	printf("a=%d, b%d", a, b);

	int c = 0;

	while( b)
	{

		c = (~a) & b;
		a = a ^ b;
		b = c << 1;
	}

	printf(" a-b=%d\n", a);
}




void b_mul(int a, int b) 
{
	printf("a=%d, b%d", a, b);

	int c = 0;
	while( b )
	{
		c = a & b;
		if( b & 1)	
		{
			c +=a;
		}
		a<<=1;
		b>>=1;
	}

	printf(" a*b=%d\n", c);

}

void b_mult_niave(int a, int b)
{
	printf("b_mult_niave: a=%d, b%d", a, b);
     

}



//   a/b
void b_div(int a, int b) 
{
	printf("a=%d, b%d", a, b);


    
    if(b == 0 || (a == INT_MIN && b == -1))
        return;
    
    int Q = 0;//Quotient
    unsigned int N = (b<0)? -b: b;
    unsigned int D = (a<0)? -a: a;
    
    
    bool sign = (b<0)^(a<0);
    
    int i = 31;
    
    while( i >=0 )
    {    
        if( (D>>i) >= N)
        {
            //D-=N;
            D -= (N<<i);
            Q = (Q<<1)| 0x01;            
        }
        else
        {
            Q<<=1;
        }
        i--;
    }
    
    if(sign)
        Q = -Q;
    
	printf(" a/b=%d\n", Q);

}



/* ----------------------------------------------------------------------------------- */
/* ---------------------------------Counting Bits------------------------------------- */
/* ----------------------------------------------------------------------------------- */
unsigned int count_bits(int i)
{
	unsigned int counter = 0;
	while(i > 0)
	{
		counter += i & 1;
		i >>= 1;
	}

	return counter;

}


/* ----------------------------------------------------------------------------------- */
/* ---------------------------------Check Endian-------------------------------------- */
/* ----------------------------------------------------------------------------------- */

bool endian(void)
{

	unsigned char arr[2] = {0x01, 0x00};

	unsigned short int x = *(unsigned short int* )arr;
	unsigned  char  uchar_x = *(unsigned char* )arr;

    if( uchar_x)
        printf("little endian\n");


	if( x)
		return true;

	return false;

}


void check_endian()
{
	unsigned int x = 1;
	char* str = "big endian";	
	char* byte_check = (char *)&x;

	if( byte_check )
	{
		str = "little endian";
	}
	printf("%s\n", str);
 
}

void check_endian2()
{
	unsigned char arr[2] = {0x01, 0x00};//declared as LE
	unsigned short int x = *(unsigned short int *) arr;

	if(x)	
		printf("little endian\n");
	else
		printf("big endian\n");
}

void check_endian3()
{
	unsigned int x = 1;
	printf ("%d", (int) (((char *)&x)[0]));
	if(x)
		printf("little endian\n");
	else
		printf("big endian\n");

}

/* ----------------------------------------------------------------------------------- */
/* --------------------------Byte Order Conversion------------------------------------ */
/* ----------------------------------------------------------------------------------- */
//big to small endian
uint32_t nthol(uint32_t netlong)
{
	return  (netlong<<24) | ( netlong<<16) | (netlong<<8) | (netlong << 0);
}

uint32_t bswap32_wrapper(uint32_t value)
{
	return __builtin_bswap32(value);
}



uint32_t nthol_32(uint32_t netlong)
{

	// network order synonymous with BE order
	//netlong::[0xi1, 0xi2, 0xi3, 0xi4]---> hostlong::[0xi4, 0xi3, 0xi2, 0xi1]
	//get host order

	//check for Big Endian
	if( !endian())
	{
		return netlong;
	}

	//else is machine is Little Endian
	
	return  (netlong<<24) | ( netlong<<16) | (netlong<<8) | (netlong << 0);

}
