

/*

Collection of various interview questions in C.

*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h> 
#include <netdb.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 


#define _GNU_SOURCE     /* To get defns of NI_MAXSERV and NI_MAXHOST */
#include <ifaddrs.h>
#include <linux/if_link.h>



//TEST FOR MAX(X,Y) macro
#define MAX(X,Y) ((X) > (Y) ?  (X) : (Y)) 

//Macro test for endian
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
	#define	L_ENDIAN 1
#else	
	#define	L_ENDIAN 0

#endif

struct _header 
{
	int h;
};


struct _data 
{
	int d;
};

struct packet 
{
	struct _header header;
	struct _data data;
};




// prototypes
struct packet* test_struct(void);

void macro_endian_test(void);
void test_struct_call(void);



// Prototypes
int gcd(int, int);
int search_gcd_arr(int[], int);


//f5 interview questions
int num_factors_opt(int);
int num_factors_niave(int n);
int num_factors_simple(int n);

int  find_arr_max(int[], size_t );
void test_f5_string(void);
void test_arr_ptr(int **);
void some_test(void);

//Protos net ip address ops
void  get_host_addr( void ); 
int interface_test(char * iface, char* sto_buf, size_t sto_buf_size);


void test_static_struct(void);

		
		
typedef struct 
{
	int a;
	int b;
}struct_s;
		
		
		
//TODO main
//TODO main
//TODO main
int main(int argc, char* argv[])
{
	int n = atoi(argv[1]);
	printf(" factors_opt(%d) = %d\n", n, num_factors_opt(n));
	printf(" factors_niave(%d) = %d\n", n, num_factors_niave(n));
	printf(" factors_simple(%d) = %d\n", n, num_factors_simple(n));
	
	return 0;
}
//TODO END MAIN 
//TODO END MAIN 
//TODO END MAIN 



//work tag@ 9090


int num_factors_niave(int n)
{
	int f = 0;
	int i = n;	
	while( i )
	{
		if( n % i == 0)
		{
			f++;
		}
		i--;
	}	

	return f;
}


int num_factors_simple(int n)
{
	if( n == 1)
	{
		return 1;
	}

	int f = 2;

	int l = (int) ceil(  (double) n /2.0);
	
	while( l>=2 )
	{
		if ( n %l  == 0)
		{
			f++;
		}
	
		l--;
	}	

	return f;
}

int num_factors_opt(int n)
{
	
	if( n < 3)
	{
		return n;
	}
	

	int f = 2;
	int l = (int) ( ceil ( sqrt( (double) n) + 0.0001));
	printf("upper limit: %d\n", l);

	while( l>=2 )
	{
		if( n % l == 0)
		{

			f += ( l*l == n)? 1: 2;
		}
		l--;
	}		
		
	return f;
}



void test_static_struct(void)
{


	struct_s s = {1,2};
	struct_s *p_s = &s;
	int *p_a = &(p_s->a);
	int *p_b = &(p_s->b);

	printf("s.a: %d\n s.b: %d\n", s.a, s.b);
	printf("struct address %p\n", (void*) &s);
	printf("s.a address %p\n", &s.a);
	printf("s.b address %p\n", &s.b);
    printf("*p_a == &(p_s->a): %p\n", p_a);
    printf("*p_b == &(p_s->b): %p\n", p_b);

	return;	
}



void macro_endian_test(void)
{
	if( L_ENDIAN)
		printf("little endian\n");
	else
		printf("big endian\n");
	return;
}



//TESTING STRUCT CREATION AND LIVENESS
//TESTING STRUCT CREATION AND LIVENESS
//TESTING STRUCT CREATION AND LIVENESS
void test_struct_call(void)
{
	struct packet* p = test_struct();
	
	printf("packet header: %d\n packet data: %d\n", p->header.h, p->data.d);
	
	free(p);
	return;

}

struct packet* test_struct(void)
{
	struct packet * p = (struct packet*) malloc(sizeof(p));

	p->header.h = 9;
	p->data.d = 10;

	return p;
} 


//TESTING GET HOST IP ADDRESS
//TESTING GET HOST IP ADDRESS
//TESTING GET HOST IP ADDRESS
 void  get_host_addr( void)
{ 
    char host_buf[256]; 
    char *ip_buf; 
    struct hostent *host_entry; 
    int hostname; 
  
    // To retrieve hostname 
    hostname = gethostname(host_buf, sizeof(host_buf)); 
  
    // To retrieve host information 
    host_entry = gethostbyname(host_buf); 
  
    // To convert an Internet network 
    // address into ASCII string 
    ip_buf = inet_ntoa(*((struct in_addr*) 
                           host_entry->h_addr_list[0])); 
  
    printf("Host IP: %s\n", ip_buf); 
  
}

 
//TESTING INTERFACE MATCHING AND EXTRACTING IP ADDRESS
int interface_test(char * iface, char* sto_buf, size_t sto_buf_size)
{
	struct ifaddrs *ifaddr, *ifa;
	int family, s, n;
	char host[NI_MAXHOST];
	if (getifaddrs(&ifaddr) == -1) 
	{
	   perror("getifaddrs");
	   exit(EXIT_FAILURE);
	}

   /* Walk through linked list, maintaining head pointer so we
      can free list later */

   	for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) 
	{
       	if (ifa->ifa_addr == NULL)
       	    continue;
	
		//Interface match	
		if(  strncmp( ifa->ifa_name, iface, sizeof(iface)) == 0)
		{
			
       	family = ifa->ifa_addr->sa_family;

       /* Display interface name and family (including symbolic
          form of the latter for the common families) */


       /* For an AF_INET* interface address, display the address */
       	if (family == AF_INET ) 
		{
			s = getnameinfo(
					ifa->ifa_addr,
				    sizeof(struct sockaddr_in), 
					host, NI_MAXHOST,
				    NULL, 0, NI_NUMERICHOST);
			if (s != 0) 
			{
				printf("getnameinfo() failed: %s\n", gai_strerror(s));
				exit(EXIT_FAILURE);
			}
			printf("iface: %s \taddress: <%s>\n", ifa->ifa_name, host);
			strncpy( sto_buf, host, sto_buf_size);
       }
  		} 
	}
   freeifaddrs(ifaddr);
	return 0;
}

/* END NETIFACE AND HOST IP TEST FUNCTIONS */


//TESTING F5 STRING OP
void test_f5_string(void)
{

	char f5[] = "F5NETWORKS";
	char* p = f5;

	printf("%s\n", p + p[0] -p[3]);
	//unsigned char a = 0xFF;
//	printf("unsigned byte: %u \n", a);

	return;
}

//TESTING ARRAY POINTER
void test_arr_ptr(int ** arr)
{

	int a00 = *(arr[0]);
	printf(" a00: %d, %d \n", a00, arr[0][1] );
	
	arr++;
	a00 = *( *arr);
	printf(" a00: %d \n", a00);
	
	arr++;
	a00 = *( *arr);
	printf(" a00: %d \n", a00);
}





void some_test(void)
{
	int arr [] = {0, 0, 0, 0};
	int arr_len = 4;

	int i = 0;
	while( i < arr_len)
	{
		*(arr + i) = (i++);
	}

	i = 0;
	while( i < arr_len)
	{
		printf("%d ", (*arr)++);
		i++;	
	}
	printf("\n");

	i = 0;

	return;
}



int find_arr_max(int arr [], size_t len)
{
	int i = 0;
	int index = -1;

	len--;

	while(i < len)
	{
		if( arr[i] < arr[i+1])
		{
			index = i;
			i = len;
		}

		i++;
	}	

	return index;
}


int gcd(int a, int b)
{
	int t = 0;

	while(b)
	{
		t = b;
		b = a % b;
		a = t;
	}	
	
	return a;
}

	
int search_gcd_arr(int arr [], int len)
{
	
	int a = *arr;
	if(len > 1)
	{
		a = arr[len--];
		while(len)
		{
			a = gcd(a, arr[len]);	
			len--;
		}
	}
	return a;
}	
