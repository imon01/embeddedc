/*
Bit clear
Bit set
Bit swap

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#include "show_bytes.h"


void bit_test(void);
char *binary_string(int x);
void bit_match(int x, int y);



void bit_set(int, int);
void bit_clear(int, int);
void bit_swap( int*x, int*y);



void left_most_bit(unsigned x);

void bit_reverse_niave(int number);
void bit_reverse(unsigned char number); 
void bit_array_reverse( int arr [], int len);

void byte_replace(int x, int i, int y);



int main(int argc, char* argv[])
{
	int x = 0xAAAAAAAA;
	int y = 0xBBBBBBBB;


    bit_clear( 9, 4);
//	int z =  (x & 0x000000FF) | (y & 0xFFFFFF00);
//	printf("z : %X \n", z);

	unsigned int a = 0xFF00;//1111.1111.0.0
	unsigned int b = 0x6600;//0110.0110.0.0 

	left_most_bit(a);
	left_most_bit(b);
    bit_reverse(9);
    bit_reverse(2);
}


//leftmost set bit
void left_most_bit(unsigned int x)
{

	int k = 0;	
	int i  = 0;
	unsigned char bits[32];


	while( i < 32)
	{
		bits[i++] = 0;
	}

	i = 31;
	
	while(x)
	{
		if( (x & 1) )
		{
			bits[i] = 1;
		}	
		i--;
		x>>=1;
	}

	i = 0;

	while( !bits[i])
	{
		i++;
	}		

	if( bits[i])
	{

		x = 0x1;
		k = 31 -i;

		
		while(k)
		{
			x<<=1;
			k--;
		}
		printf("leftmost bit x: %X\n",x);
	}

	

}


void byte_replace(int x, int i, int y)
{
	
	int b = 0xFF;
	
	while(i)
	{
		//shift bits 7 places left to next byte start position.
		b<<=7;
		printf("b: %X\n", b); 
		i--;	
	}	

	b = b & y;

	x = x | b;

	printf("x: %X\n", x); 
}



void bit_clear(int number, int which_bit)
{
    which_bit--;
    int bit = 1 << which_bit;

    printf("before clear: %d\n", number);
    number ^= bit;

    printf("after clear: %d\n", number);

}

void bit_set(int w, int m)
{

	unsigned char b = 0;
	int z = 0;
	while(m)
	{	
		b = (w & m & 1) ? 1 : 0;

		z = b | z;
		z <<=1;
		m>>1;
	}


    		
}

/*-------------------START IN-PLACE BIT ARRAY REVERSE-----------------*/
/*-------------------START IN-PLACE BIT ARRAY REVERSE-----------------*/
/*-------------------START IN-PLACE BIT ARRAY REVERSE-----------------*/



void bit_reverse_niave(int number)
{
    printf("before reverse: %d\n", number);

    int length = sizeof(int)*8;
    int left_bit = 0;
    int right_bit = 0;
    int iterations = length/2;
    length--;
    int right_shift = 0;
    printf("number of bits: %d\n", length);

    while( iterations--)
    {
        left_bit = number>>length;
        //printf("last bit: %d", left_bit); 
        right_bit = (number<<right_shift);
        right_bit <<=length;
        number ^= right_bit;
        number ^= left_bit; 
        right_shift++;  
    }
    printf("after reverse: %d\n", number);

}

void bit_reverse(unsigned char number)
{

    int reverse = 0; 
    size_t left_shift = sizeof(number)*8 - 1;
    size_t right_shift =  0;
    int temp = 0; 

    printf("before reverse: %d\n", number);
    while( number )
    {
        reverse |= ( (1&number) << left_shift);
        left_shift--;
        number >>=1;
    }
    printf("after reverse: %d\n", reverse);

}

void bit_swap( int*x, int*y)
{

}

void bit_array_reverse( int arr [], int len)
{
	

}



/*-------------------END IN-PLACE BIT ARRAY REVERSE-----------------*/
/*-------------------END IN-PLACE BIT ARRAY REVERSE-----------------*/
/*-------------------END IN-PLACE BIT ARRAY REVERSE-----------------*/












/* -------------------START BIT MATCHING------------------*/
/* -------------------START BIT MATCHING------------------*/
/* -------------------START BIT MATCHING------------------*/

void bit_test(void)
{

	int x = 0x68;
	int y = 0xAF;
	bit_match( y, x);
}

void bit_match(int x, int y)
{

	int z = 0;
	int zz  = x & y;
	
	printf("mask match (zz): %X \n", zz);	

	unsigned char b = 0;	

	while(x)
	{
	
		b =  (  ( x & y & 1) | ( ~x & ~y & 1)   ) ? 1: 0;
		
		if( b)
			printf("1 ");	
		z = z | b;
		z <<= 1;
		x>>=1;
		y>>=1;
	

	}

			printf("\n");	
	
	char * str_z = binary_string(z	);

	
	printf("bit match (z): %X \n", z);	
	printf("bit match: %s \n", str_z);

	free(str_z);

}


char * binary_string(int x)
{

	int i = 0;
	int bit_len = 8 * sizeof(int);
	
	char * s = malloc(sizeof(int) + 1);	
	s[bit_len] = '\0';


	while(i < bit_len)
	{
		s[i] = '0';
		i++;
	}	

	i = bit_len -1;
	while(x)
	{
		if( x & 1)
		{
			s[i] = '1';
		}
		i--;
		x >>=1;	
	}


    return s;
}

/* -------------------END BIT MATCHING------------------*/
/* -------------------END BIT MATCHING------------------*/
/* -------------------END BIT MATCHING------------------*/



