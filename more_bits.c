#include <stdio.h>
#include <stdlib.h>


typedef struct _ll_node * ll_node;

typedef struct _ll_node
{
    int value;
    ll_node next;
}_ll_node;


typedef struct _ll_list * ll_list;

typedef struct _ll_list
{
    ll_node head;
    unsigned int size;
}_ll_list;






void test_ll_list(ll_list list);
void powers_of_tow(void);
void test_char_sto_in_int32(void);
unsigned int pack(char* c_array, size_t len);
char* unpack(unsigned int storage_int32, char* storage_str, size_t len);



int main(int argc, char* argv[])
{

    ll_list list = (ll_list) malloc( sizeof(list));
    list->head = (ll_node) malloc( sizeof(list->head));
    list->size++;
    list->head->value= 2;
    list->head->next = 0; 
    
    test_ll_list(list);
    free(list->head);
    free(list);
}


void test_ll_list(ll_list list)
{

    printf("base address: %p\n", &list);
    printf("list head address: %p\n", list->head);
    printf("head valie: %d\n", list->head->value);
    printf("list size: %d\n", list->size);

}


void powers_of_tow(void)
{

    unsigned int c = 1;

    //size_t limit = sizeof(c)*8 - 1; //causes infinite loop
    int limit = (int)sizeof(c)*8 ;
    printf("limit=%d\n", limit); 
    limit--; 
    printf("limit=%d\n", limit); 
   
    while(limit> 0)
    { 
        printf("%u ", c<<limit);
        printf("limit=%d\n", limit); 
        limit-=2;
    } 
    printf("\n");

}


void test_char_sto_in_int32(void)
{
    char ca [] = {'a', 'b', 'c', 'd'};
    char ac [] = {'0', '0', '0', '0'};
    unsigned int buffer_int =  pack(ca, 4);
    
    printf("back to char (from int): %s\n", unpack(buffer_int, ac, 4));

}



unsigned int pack(char *c_array, size_t len)
{
    unsigned int storage_array = 0;
   

    for(int i = 0; i < len; i++)
    { 
        storage_array |= (c_array[i]<<8*i);
    } 


    printf("character in first 8 bits: %c\n", storage_array<<16);

    return storage_array;
}


char* unpack(unsigned int storage_int, char *return_str, size_t len)
{
    if (!return_str)
        return 0;
    int i = 0;
    while (i < len) {
        return_str[i++] = storage_int >> 8 * i;
    }
    return return_str;
}
